#!/usr/bin/env zsh
cat "$1" | cut -d',' -f2 | sed 's/http.*$//;/^\s+$/d' | head -n "$2"
